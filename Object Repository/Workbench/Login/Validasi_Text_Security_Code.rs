<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Validasi_Text_Security_Code</name>
   <tag></tag>
   <elementGuidId>e110889b-5727-40f4-b698-6ee1d7681261</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[contains(.,&quot;Security Code&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'authcode']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>authcode</value>
   </webElementProperties>
</WebElementEntity>

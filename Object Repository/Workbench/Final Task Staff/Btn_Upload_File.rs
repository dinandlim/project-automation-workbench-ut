<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Upload_File</name>
   <tag></tag>
   <elementGuidId>01adb266-bd80-4d66-bde0-b1134a01933a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'uploadBtnOther_7']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@title=&quot;Upload File&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>uploadBtnOther_7</value>
   </webElementProperties>
</WebElementEntity>

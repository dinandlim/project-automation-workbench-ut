<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Textfield_Tax_ID</name>
   <tag></tag>
   <elementGuidId>5bb5aad5-19a3-41cd-a4a1-7eb58a0e2e19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'ap_tax_id_no']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ap_tax_id_no</value>
   </webElementProperties>
</WebElementEntity>

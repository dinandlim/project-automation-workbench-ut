<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Textfield_Loan_Disbursement_Security_Code</name>
   <tag></tag>
   <elementGuidId>76783c60-84a4-4f61-9bea-b8a3ff5a546f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'multi_auth_code']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>multi_auth_code</value>
   </webElementProperties>
</WebElementEntity>

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


DBData data = findTestData('Workbench/Application Data')

String Loan_Id = GlobalVariable.txt_loan_id

def loan = ('\'' + Loan_Id) + '\''

data.query = data.query.replace('\'_LOAN_ID_\'', loan)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Apli_Id = data.getValue(1, index)

    Apli_Ap_Id = data.getValue(2, index)

    Apli_Status = data.getValue(3, index)

    if (Apli_Status == 'CS') {
        //CS activity
        WebUI.callTestCase(findTestCase('Workbench/WB Login/WB Login CS'), [:], FailureHandling.STOP_ON_FAILURE)

        WebUI.callTestCase(findTestCase('Workbench/WB Job Activity/WB CS Job Activity'), [:], FailureHandling.STOP_ON_FAILURE)

        //Staff activity
        WebUI.callTestCase(findTestCase('Workbench/WB Login/WB Login Staff'), [:], FailureHandling.STOP_ON_FAILURE)

        WebUI.callTestCase(findTestCase('Workbench/WB Job Activity/WB Staff Job Activity'), [:], FailureHandling.STOP_ON_FAILURE //WebUI.verifyTextPresent('Failed = Login gagal cek apli status di DB', false, FailureHandling.STOP_ON_FAILURE)
            ) //WebUI.callTestCase(findTestCase('Workbench/WB Login Security Code/WB Login Security Code Manager'), [:], FailureHandling.STOP_ON_FAILURE)
    } else if (Apli_Status == 'ST') {
        WebUI.callTestCase(findTestCase('Workbench/WB Login/WB Login Staff'), [:], FailureHandling.STOP_ON_FAILURE)

        WebUI.callTestCase(findTestCase('Workbench/WB Job Activity/WB Staff Job Activity'), [:], FailureHandling.STOP_ON_FAILURE)
    } else if (Apli_Status == 'A') {
        WebUI.callTestCase(findTestCase('Workbench/WB Login/WB Login Manager'), [:], FailureHandling.STOP_ON_FAILURE)
    } else {
        CustomKeywords.'set.MarkFailAndStop.markFailedAndStop'('Login gagal cek apli status di DB')
    }
}


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Workbench/Data Login Manager')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
	username_Manager = data.getValue(1, index)
	
	DBData dataAuth = findTestData('Workbench/Data Authentication Manager')
	
	String user_Manager = username_Manager
	
	def lbl_user_Manager = ('\'' + user_Manager) + '\''
	
	dataAuth.query = dataAuth.query.replace('\'_USER_MANAGER_\'', lbl_user_Manager)
	
	dataAuth.fetchedData = dataAuth.fetchData()
	
	for (def index2 : (1..dataAuth.getRowNumbers())) {
		Loan_Security_Code = dataAuth.getValue(1, index2)
	
		WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Textfield_Loan_Disbursement_Security_Code'),
			0)
	
		WebUI.setText(findTestObject('Workbench/Final Task Manager/Textfield_Loan_Disbursement_Security_Code'), Loan_Security_Code)
	
		CustomKeywords.'get.Screencapture.getScreenShot'('WB-Loan_Disbursement_Security.jpg')
	
		WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Btn_Apply_Disbursement'), 0)
	
		WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Apply_Disbursement'), FailureHandling.STOP_ON_FAILURE)
	}
}
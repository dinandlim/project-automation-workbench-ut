import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Workbench/Dashboard/Text_Welcome_to_WB'), 0)

WebUI.verifyElementPresent(findTestObject('Workbench/Dashboard/Text_Welcome_to_WB'), 0)

CustomKeywords.'get.Screencapture.getScreenShot'('WB-Dashboard.jpg')

WebUI.waitForElementVisible(findTestObject('Workbench/Menu/Btn_Navigation_Menu'), 0)

WebUI.mouseOver(findTestObject('Workbench/Menu/Btn_Navigation_Menu'))

WebUI.waitForElementClickable(findTestObject('Workbench/Menu/Btn_Application_Workbench'), 0)

WebUI.click(findTestObject('Workbench/Menu/Btn_Application_Workbench'), FailureHandling.STOP_ON_FAILURE)


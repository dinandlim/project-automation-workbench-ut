import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementClickable(findTestObject('Workbench/Filter/Btn_Setting_Filter'), 0)

WebUI.click(findTestObject('Workbench/Filter/Btn_Setting_Filter'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Workbench/Filter/Btn_Filter_By'), 0)

WebUI.click(findTestObject('Workbench/Filter/Btn_Filter_By'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Workbench/Filter/Textfield_Filter'), 0)

WebUI.setText(findTestObject('Workbench/Filter/Textfield_Filter'), txt_filter_by)

WebUI.waitForElementVisible(findTestObject('Workbench/Filter/Btn_Pilih_Filter'), 0)

WebUI.click(findTestObject('Workbench/Filter/Btn_Pilih_Filter'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Workbench/Filter/Textfield_Loan_ID'), 0)

WebUI.setText(findTestObject('Workbench/Filter/Textfield_Loan_ID'), GlobalVariable.txt_loan_id)

CustomKeywords.'get.Screencapture.getScreenShot'('WB_Filter.jpg')

WebUI.waitForElementClickable(findTestObject('Workbench/Filter/Btn_Submit_Filter'), 0)

WebUI.click(findTestObject('Workbench/Filter/Btn_Submit_Filter'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Workbench/Filter/Btn_Setting_Filter'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementNotVisible(findTestObject('Workbench/Text_Loading_Filter'), 0)


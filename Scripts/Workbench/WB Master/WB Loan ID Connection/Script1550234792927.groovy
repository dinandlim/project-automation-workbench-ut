import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData TempLoanData = findTestData('Workbench/Data Temp Loan ID')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

TempLoanData.query = TempLoanData.query.replace('\'_EXECUTOR_\'', lbl_executor)

TempLoanData.fetchedData = TempLoanData.fetchData()

for(def TempLoanIndex : (1..TempLoanData.getRowNumbers())){
	
	Temp_LoanID = TempLoanData.getValue(1, TempLoanIndex)
	
	GlobalVariable.txt_loan_id = Temp_LoanID
}
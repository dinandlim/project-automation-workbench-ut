import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Data Authentication Manager')

String user_Manager = Username_Manager

def lbl_user_Manager = ('\'' + user_Manager) + '\''

data.query = data.query.replace('\'_USER_MANAGER_\'', lbl_user_Manager)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
	Text_Auth_Manager = data.getValue(1, index)

	WebUI.waitForElementVisible(findTestObject('Workbench/Login/Validasi_Text_Security_Code'), 0, FailureHandling.OPTIONAL)

	Boolean lblAuth_Manager = WebUI.verifyElementVisible(findTestObject('Workbench/Login/Validasi_Text_Security_Code'),
		FailureHandling.OPTIONAL)

	if (lblAuth_Manager == true) {
		WebUI.waitForElementVisible(findTestObject('Workbench/Login/Textfield_Login_Auth_Code'), 0)

		WebUI.setText(findTestObject('Workbench/Login/Textfield_Login_Auth_Code'), Text_Auth_Manager)

		CustomKeywords.'get.Screencapture.getScreenShot'('WB-Security_Code_CS.jpg')

		WebUI.click(findTestObject('Workbench/Login/Btn_Submit_Auth_Code'))
	} else {
		print('Login Tanpa Security')
	}
}


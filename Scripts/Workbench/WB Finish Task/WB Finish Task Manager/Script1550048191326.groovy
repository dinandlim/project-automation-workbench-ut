import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

int j = 0

DBData data = findTestData('Workbench/Upload Data Manager')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Upload_KTP = data.getValue(1, index)

    Text_Tax_ID = data.getValue(2, index)

    Upload_Tax_ID = data.getValue(3, index)

    Upload_Monthly_Income = data.getValue(4, index)

    Upload_Cust_Photo = data.getValue(5, index)

    Text_Family_ID = data.getValue(6, index)

    Upload_Family_ID = data.getValue(7, index)

    Surveyor_Doc_Date = data.getValue(8, index)

    Upload_Surveyor_Doc = data.getValue(9, index)

    Upload_Signed_Agreement = data.getValue(10, index)

    text_LoanId = new TestObject('')

    text_LoanId.addProperty('xpath', ConditionType.EQUALS, ('//table[@id=\'sample-table-1\']/tbody/tr/td[text()=\'' + GlobalVariable.txt_loan_id) + 
        '\']')

    WebUI.waitForElementVisible(text_LoanId, 0)

    WebUI.verifyElementPresent(text_LoanId, 0)

    CustomKeywords.'get.Screencapture.getScreenShot'('WB-Selected_Task_Manager.jpg')

    WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Btn_Docs'), 0)

    WebUI.waitForElementClickable(findTestObject('Workbench/Final Task Manager/Btn_Docs'), 0)

    WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Docs'))

    WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Upload_Personal_ID'), 0)

    CustomKeywords.'get.Screencapture.getScreenShot'('WB_Upload_File_Manager.jpg')

    CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Manager/Upload_Personal_ID'), 
        Upload_KTP, 0)

    WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Textfield_Tax_ID'), 0)

    WebUI.doubleClick(findTestObject('Workbench/Final Task Manager/Textfield_Tax_ID'), FailureHandling.STOP_ON_FAILURE)

    WebUI.setText(findTestObject('Workbench/Final Task Manager/Textfield_Tax_ID'), Text_Tax_ID)

    CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Manager/Upload_Tax_ID'), Upload_Tax_ID, 
        0)

    CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Manager/Upload_Monthly_Income'), 
        Upload_Monthly_Income, 0)

    CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Manager/Upload_Cust_Photo'), 
        Upload_Cust_Photo, 0)

    WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Textfield_Family_ID'), 0)

    WebUI.doubleClick(findTestObject('Workbench/Final Task Manager/Textfield_Family_ID'), FailureHandling.STOP_ON_FAILURE)

    WebUI.setText(findTestObject('Workbench/Final Task Manager/Textfield_Family_ID'), Text_Family_ID)

    CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Manager/Upload_Family_ID'), Upload_Family_ID, 
        0)

    WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Date_Survey'))

    WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Date_Survey'))

    CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Manager/Upload_Surveyor_Doc'), 
        Upload_Surveyor_Doc, 0)

    CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Manager/Upload_Signed_Agreement'), 
        Upload_Signed_Agreement, 0)
}

WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Ok'))

WebUI.waitForElementNotVisible(findTestObject('Workbench/Text_Loading_Filter'), 0)

DBData data2 = findTestData('Workbench/Data Login Manager')

String executor2 = GlobalVariable.txt_executor

def lbl_executor2 = ('\'' + executor) + '\''

data2.query = data2.query.replace('\'_EXECUTOR_\'', lbl_executor2)

data2.fetchedData = data2.fetchData()

for (def index2 : (1..data2.getRowNumbers())) {
    user_Manager = data2.getValue(1, index2)

    DBData data3 = findTestData('Workbench/Data Asignee Manager')

    String bu_Name = user_Manager

    def lbl_bu_Name = ('\'' + bu_Name) + '\''

    data3.query = data3.query.replace('\'_BU_NAME_MANAGER_\'', lbl_bu_Name)

    data3.fetchedData = data3.fetchData()

    for (def index3 : data3.getRowNumbers()) {
        Bu_ID = data3.getValue(1, index3)

        WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Assignee_Manager'))

        WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Btn_Assign'), 0)

        WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Dropdown_List_Manager'), 0)

        WebUI.selectOptionByValue(findTestObject('Workbench/Final Task Manager/Dropdown_List_Manager'), Bu_ID, false)

        WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Assign'))
    }
}

WebUI.waitForElementNotVisible(findTestObject('Workbench/Final Task Manager/Btn_Assign'), 0)

WebUI.waitForElementNotVisible(findTestObject('Workbench/Final Task Manager/Text_Title_Assignee_Manager'), 0)

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Btn_Verify'), 0)

WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Verify'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Btn_Doc_Verification'), 0)

WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Doc_Verification'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Status_Verified'), 0)

WebUI.mouseOver(findTestObject('Workbench/Final Task Manager/Dropdown_Notification'))

WebUI.callTestCase(findTestCase('Workbench/WB Master/WB Loan Disbursement Task'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Manager/Btn_Disbursing'), 0)

lbl_Loan_ID = new TestObject('')

lbl_Loan_ID.addProperty('xpath', ConditionType.EQUALS, ('//table[@id=\'sample-table-1\']/tbody/tr/td[text()=\'' + GlobalVariable.txt_loan_id) + 
    '\']')

Boolean Loan_ID = WebUI.verifyElementVisible(lbl_Loan_ID, FailureHandling.OPTIONAL)

if (Loan_ID == false) {
    for (Loan_ID; Loan_ID != true; j++) {
        Loan_ID = WebUI.verifyElementVisible(lbl_Loan_ID, FailureHandling.OPTIONAL)

        if (Loan_ID == false) {
            WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Next'), FailureHandling.STOP_ON_FAILURE)
        } else {
            Selected_Disbursing = new TestObject('')

            Selected_Disbursing.addProperty('xpath', ConditionType.EQUALS, ('//table[@id=\'sample-table-1\']/tbody/tr/td[text()=\'' + 
                GlobalVariable.txt_loan_id) + '\']/preceding-sibling::td/div/label/div/ins')

            WebUI.click(Selected_Disbursing, FailureHandling.STOP_ON_FAILURE)

            WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Disbursing'))

            WebUI.callTestCase(findTestCase('Workbench/WB Master/WB Loan Disbursement Security'), [:], FailureHandling.STOP_ON_FAILURE)
        }
    }
} else {
    Selected_Disbursing = new TestObject('')

    Selected_Disbursing.addProperty('xpath', ConditionType.EQUALS, ('//table[@id=\'sample-table-1\']/tbody/tr/td[text()=\'' + 
        GlobalVariable.txt_loan_id) + '\']/preceding-sibling::td/div/label/div/ins')

    WebUI.click(Selected_Disbursing, FailureHandling.STOP_ON_FAILURE)

    CustomKeywords.'get.Screencapture.getScreenShot'('WB-Select_Disbursement.jpg')

    WebUI.click(findTestObject('Workbench/Final Task Manager/Btn_Disbursing'))

    WebUI.callTestCase(findTestCase('Workbench/WB Master/WB Loan Disbursement Security'), [:], FailureHandling.STOP_ON_FAILURE)
}


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

text_LoanId = new TestObject('')

text_LoanId.addProperty('xpath', ConditionType.EQUALS, ('//table[@id=\'sample-table-1\']/tbody/tr/td[text()=\'' + GlobalVariable.txt_loan_id) +
	'\']')

WebUI.waitForElementVisible(text_LoanId, 0)

WebUI.verifyElementPresent(text_LoanId, 0)

CustomKeywords.'get.Screencapture.getScreenShot'('WB-Selected_Task_CS.jpg')

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task CS/Btn_Calls_and_Survey'), 0)

WebUI.click(findTestObject('Workbench/Final Task CS/Btn_Calls_and_Survey'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task CS/Dropdown_List_Applicant'), 0)

WebUI.selectOptionByValue(findTestObject('Workbench/Final Task CS/Dropdown_List_Applicant'), 'Answered', false)

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task CS/Dropdown_List_Office'), 0)

WebUI.selectOptionByValue(findTestObject('Workbench/Final Task CS/Dropdown_List_Office'), 'Answered', false)

WebUI.waitForElementVisible(findTestObject('Workbench/Final Task CS/Dropdown_List_Family'), 0)

WebUI.selectOptionByValue(findTestObject('Workbench/Final Task CS/Dropdown_List_Family'), 'Answered', false)

CustomKeywords.'get.Screencapture.getScreenShot'('WB-Answered_CS.jpg')

WebUI.click(findTestObject('Workbench/Final Task CS/Btn_Ready_Yes'))

WebUI.click(findTestObject('Workbench/Final Task CS/Btn_Save'))
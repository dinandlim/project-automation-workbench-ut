import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Workbench/Upload Data Staff')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
	Date_Doc = data.getValue(1, index)
	
	Upload_Doc = data.getValue(2, index)

	text_LoanId = new TestObject('')

	text_LoanId.addProperty('xpath', ConditionType.EQUALS, ('//table[@id=\'sample-table-1\']/tbody/tr/td[text()=\'' + GlobalVariable.txt_loan_id) +
		'\']')

	WebUI.waitForElementVisible(text_LoanId, 0)

	WebUI.verifyElementPresent(text_LoanId, 0)

	CustomKeywords.'get.Screencapture.getScreenShot'('WB-Selected_Task_Staff.jpg')

	WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Staff/Btn_Docs'), 0)

	WebUI.waitForElementClickable(findTestObject('Workbench/Final Task Staff/Btn_Docs'), 0)

	WebUI.click(findTestObject('Workbench/Final Task Staff/Btn_Docs'), FailureHandling.STOP_ON_FAILURE)

	WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Staff/Btn_Kalender'), 0)

	WebUI.waitForElementClickable(findTestObject('Workbench/Final Task Staff/Btn_Kalender'), 0)

	WebUI.click(findTestObject('Workbench/Final Task Staff/Btn_Kalender'), FailureHandling.STOP_ON_FAILURE)

	WebUI.click(findTestObject('Workbench/Final Task Staff/Btn_Kalender'), FailureHandling.STOP_ON_FAILURE)

	WebUI.waitForElementVisible(findTestObject('Workbench/Final Task Staff/Btn_Upload_File'), 0 //WebUI.setText(findTestObject('Workbench/Final Task Staff/Btn_Upload_File'), Upload_Doc)
		)

	//WebUI.setText(findTestObject('Workbench/Final Task Staff/Btn_Upload_File'), Upload_Doc)
	CustomKeywords.'uploadfile.UploadFile.uploadFile'(findTestObject('Workbench/Final Task Staff/Btn_Upload_File'), Upload_Doc,
		0 //WebUI.click(findTestObject('Workbench/Final Task Staff/Btn_Upload_File'), FailureHandling.STOP_ON_FAILURE)
		//WebUI.sendKeys(findTestObject('Workbench/Final Task Staff/Btn_Upload_File'), Keys.chord(Keys.ENTER))
		) //WebUI.sendKeys(findTestObject('Workbench/Final Task Staff/Btn_Upload_File'), Keys.chord( Keys.META, Keys.SHIFT, 'G'))

	CustomKeywords.'get.Screencapture.getScreenShot'('WB-View_Process_Doc.jpg')
}

//WebUI.delay(5)
WebUI.click(findTestObject('Workbench/Final Task Staff/Btn_Ok'))

WebUI.waitForElementNotVisible(findTestObject('Workbench/Text_Loading_Filter'), 0)
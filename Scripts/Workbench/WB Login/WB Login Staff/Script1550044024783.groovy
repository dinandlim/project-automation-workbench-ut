import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData("Workbench/Data Login Staff")

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for(def index :(1..data.getRowNumbers())){
	
	Username_Staff = data.getValue(1, index)
	
	Password_Staff = data.getValue(2, index)
	
	WebUI.waitForElementVisible(findTestObject('Workbench/Login/Textfield_User'), 0)
	
	WebUI.setText(findTestObject('Workbench/Login/Textfield_User'), Username_Staff)
	
	WebUI.waitForElementVisible(findTestObject('Workbench/Login/Textfield_Password'), 0)
	
	WebUI.setText(findTestObject('Workbench/Login/Textfield_Password'), Password_Staff)
	
	CustomKeywords.'get.Screencapture.getScreenShot'('WB-Login_Staff.jpg')
	
	WebUI.waitForElementClickable(findTestObject('Workbench/Login/Btn_Login'), 0)
	
	WebUI.click(findTestObject('Workbench/Login/Btn_Login'), FailureHandling.STOP_ON_FAILURE)
	
	DBData dataAuth = findTestData('Workbench/Data Authentication Staff')
	
	String user_Staff = Username_Staff
	
	def lbl_user_Staff = ('\'' + user_Staff) + '\''
	
	dataAuth.query = dataAuth.query.replace('\'_USER_STAFF_\'', lbl_user_Staff)
	
	dataAuth.fetchedData = dataAuth.fetchData()
	
	for (def index2 : (1..dataAuth.getRowNumbers())) {
		Text_Auth_Staff = dataAuth.getValue(1, index2)
	
		WebUI.waitForElementVisible(findTestObject('Workbench/Login/Validasi_Text_Security_Code'), 0, FailureHandling.OPTIONAL)
	
		Boolean lblAuth_Staff = WebUI.verifyElementVisible(findTestObject('Workbench/Login/Validasi_Text_Security_Code'), FailureHandling.OPTIONAL)
	
		if (lblAuth_Staff == true) {
			WebUI.waitForElementVisible(findTestObject('Workbench/Login/Textfield_Login_Auth_Code'), 0)
	
			WebUI.setText(findTestObject('Workbench/Login/Textfield_Login_Auth_Code'), Text_Auth_Staff)
	
			CustomKeywords.'get.Screencapture.getScreenShot'('WB-Security_Code_CS.jpg')
	
			WebUI.click(findTestObject('Workbench/Login/Btn_Submit_Auth_Code'))
		} else {
			CustomKeywords.'set.testing.markPassed'('Login Tanpa Security')
		}
	}
	
	
}

